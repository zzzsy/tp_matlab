%% 1.1

x = 0:0.1:2 * pi;

y_sin = sin(x);
y_cos = cos(x);

plot(x, y_sin, 'r-');

grid on;

title('Courbe sin(x) et cos(x)');
xlabel('x');
ylabel('y');

% hold on;
plot(x, y_cos, 'go');

%% 1.2

x = 0:0.01:2 * pi;

y1 = exp(-x / 3) .* sin(3 * x);

y2 = exp(-x / 3);

plot(x, y1, '--b');

hold on
plot(x, y2, '-r');
plot(x, -y2, '-r');
hold off

legend('y = e^{-x/3}sin(3x)', 'y = e^{-x/3}');
xlabel('x');
ylabel('y');

%% 2.1
t = 0:0.01:2 * pi;
x = cos(t) .^ 3;
y = sin(t) .^ 3;
plot(x, y);
axis equal;

%% 2.2
[x, y] = meshgrid(-2:0.01:0.5, 0:0.01:2);

z = exp(x) + sin(x .* y);

contour(x, y, z, [0 0], 'LineWidth', 2);
xlabel('x');
ylabel('y');
title('Courbe d''équation implicite ex + sin(xy) = 0');

%% 2.3
fplot(@(x) cos(tan(pi * x)), [-1, 1]);
xlabel('x');
ylabel('y');
title('f(x) = cos(tan(\pi x))');

%% 2.4
x = 0:0.01:2 * pi;
y1 = tanh(x);
y2 = sin(x);
y3 = cos(x);

figure
plot(x, y1, 'LineWidth', 2)
hold on
plot(x, y2, 'LineWidth', 2)
plot(x, y3, 'LineWidth', 2)

title('Graphique des fonctions tanh(x), sin(x) et cos(x)')
xlabel('x')
ylabel('y')
legend('tanh(x)', 'sin(x)', 'cos(x)')
grid on;

%% 3.1
t = 0:0.01:2 * pi;
x = sin(t);
y = cos(t);
z = t;
plot3(x, y, z);
xlabel('x');
ylabel('y');
zlabel('z');
title('Courbe paramétrée : x = sin(t), y = cos(t), z = t');

%% 3.2
t = 0:0.01:2 * pi;
x = sin(t);
y = cos(t);
z = t .* sin(t) .* cos(t);
plot3(x, y, z);
xlabel('x');
ylabel('y');
zlabel('z');
title('Courbe paramétrée : x = sin(t), y = cos(t), z = t*sin(t)*cos(t)');

%% 3.3
[X, Y] = meshgrid(-3:0.1:3, 1:0.1:5);

Z = (X + Y) .^ 2;

mesh(X, Y, Z);

title('Surface de l''équation Z = (X + Y)^2');
xlabel('X');
ylabel('Y');
zlabel('Z');

%% 4.1
x = 0:0.01:2 * pi;
y = sin(x);
plot(x, y);
grid on;
xlabel('Variable X');
ylabel('Fonction Y');
title('Schéma de la fonction sin x');

%% 4.2
x = 0:0.01:2 * pi;
y = sin(x);
z = cos(x);
plot(x, y, 'b-', x, z, 'r-');
legend('sin(x)', 'cos(x)');
xlabel('x');
ylabel('y, z');
title('Courbes de sin(x) et cos(x)');

%% 4.3
subplot(2, 2, 1);
subplot(2, 2, 2);
subplot(2, 2, 3);
subplot(2, 2, 4);

subplot(2, 2, 1);
t = 0:0.01:2 * pi;
y = sin(t);
plot(t, y);
title('y = sin(t)');

subplot(2, 2, 2);
z = cos(t);
plot(t, z);
title('z = cos(t)');

subplot(2, 2, 3);
x = 0:0.01:2 * pi;
a = sin(x) .* cos(x);
plot(x, a);
title('a = sin(x) cos(x)');

subplot(2, 2, 4);
b = cos(x) ./ sin(x);
plot(x, b);
title('b = cos(x)/sin(x)');

% x = 0:0.01:2 * pi;
% y = cos(x) ./ sin(x);
% plot(x, y);

% a = linspace(0, 2 * pi);
% b = cos(c) ./ sin(c);
% plot(c, d);

%% 4.4
theta = 0:0.01:2 * pi;

r = sin(2 * theta) .* cos(2 * theta);

x = r .* cos(theta);
y = r .* sin(theta);

plot(x, y);
axis equal;
