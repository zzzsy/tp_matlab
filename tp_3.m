% exo1
f = @(x1,x2) 100*(x1 - x2)^2 + (1 - x1)^2;
f(1,2)

% exo2
% 2.1
n = linspace(1,10,10);
xn = sin(n*pi/10);
disp(xn);

% 2.2
tau = 0.1125;
t = log(2) / log(1 + r);
fprintf('Il faut %.2f années doubler.\n', t);

% 2.3
funcition y = fun1(x)
    if x > 1
        y = x^2 + 1;
    else
        y = 2*x;
    end
end

fun1(2)
fun1(-1)

% 2.4
function y = fun2(x)
    if x > 1
        y = x^2 + 1;
    elseif x > 0
        y = 2*x;
    else
        y = x^3;
    end
end

fun2(2)
fun2(.5)
fun2(-1)

% exo3
% 3.1
function y=compte(chaine,carac)
    y = 0;
    for i = 1:length(chaine)
        if chaine(i) == carac
            y = y + 1;
        end
    end
end

% 3.2
function histogramme_voyelles
    phrase = 'nous travaillons';
    voyelles = 'aeiouy';
    n = length(voyelles);
    L = [];
    for i = 1:n
        L = [L, compte(phrase,voyelles(i))];
    end
    fprintf('phrase = %s\n', phrase);
    fprintf('voyelles = %s\n', voyelles);
    disp(L);
end

% exo4
function y = somme(n)
    chaine = num2str(n);
    L = length(chaine);
    som = 0;
    for i = 1:L
        som = som + str2num(chaine(i));
    end
    y = som;
end

% exo5
% 5.1
function y = lecture(p)
    chaine = num2str(p);
    y = '';
    i = 1;
    while i <= length(chaine)
        x = chaine(i);
        compteur = 1;
        while i + compteur <= length(chaine) && chaine(i + compteur) == x
            compteur = compteur + 1;
        end
        y = strcat(y, num2str(compteur), x);
        i = i + compteur;
    end
    y = str2num(y);
end

% 5.2
function suite_Conway()
    N=6;
    L=zeros(1,N+1);
    u=1;
    L(1)=u;
    for i = 2:N
        L(i) = lecture(L(i-1));
    end
    disp(L);
end

% exo6
% 6.1
function y = parfait(n)
    div = 1;
    for i = 2:sqrt(n)
        if mod(n,i) == 0
            div = [div, i, n/i];
        end
    end
    y = (sum(unique(div)) == n);
end

%6.2
function liste_parfait()
    N = 10000;
    L=[];
    for i = 1:N
        if parfait(i)
            L = [L;i];
        end
    end
    disp(L);
end

% exo7
function y = divEuclide(a,b)
    q = 0;
    r = a;
    while r >= b
        q = q + 1;
        r = r - b;
    end
    y = [q,r];
end

% exo8
function y = testpremier(n)
    if n < 2
        y = 0;
    elseif n == 2 || n == 3
        y = 1;
    elseif mod(n,2) == 0 || mod(n,3) == 0
        y = 0;
    else
        i = 5;
        while i*i <= n
            if mod(n,i) == 0 || mod(n,i+2) == 0
                y = 0;
                return
            end
            i = i + 6;
        end
        y = 1;
    end
end
