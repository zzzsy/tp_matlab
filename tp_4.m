%% in Octave using
% pkg load symbolic


%% Exo 1
%% 1.1
syms u(t)
eqn = diff(u) == 1 + u^2;
dsolve(eqn)

%% 1.2
syms y(x)
eqn = diff(y,x,2) + 4*diff(y) + 29*y == 0;
cond = [y(0) == 0, diff(y)(0) == 15];
ySol(x) = dsolve(eqn, cond);
ySol(x) = simplify(ySol(x))

%% 1.3
syms x(t) y(t) z(t)
eqns = [diff(x) == 2*x-3*y+3*z,
        diff(y) == 4*x-5*y+3*z,
        diff(z) == 4*x-4*y+2*z];
S = dsolve(eqns)


%% Exo 2
%% 2.1
dxdt = @(t,x) [x(2); 1000*(1-x(1)) * x(2) + x(1)];
[t, sol] = ode15s(dxdt, [0 3000], [2 0]); %% or ode23s etc.
plot(t,sol)

%% 2.2
function yprime = myode(t, y)
    y1 = y(1);
    y2 = y(2);
    y3 = y(3);
    yprime = [y2*y3; -y1*y3; 0.51*y1*y2];
end
    
[t, y] = ode45(@myode, [0, 12], [0, 1, 1]);
plot(t, y(:,1), '-', t, y(:,2), '*', t, y(:,3), '+');
legend('y1', 'y2', 'y3');


%% Exo 3
%% 3.1
syms y(x)
eqn = diff(y) - y + 2*x/y == 0;
cond = [y(0) == 1];
yS(x) = dsolve(eqn, cond)

%% 3.2
function [x,y] = euler1(f,x0,y0,h,xn)
    x = x0:h:xn;
    y = zeros(size(x));
    y(1) = y0;
    for i = 1:length(x)-1
        y(i+1) = y(i) + h*f(x(i),y(i));
    end
end

%% 3.3
function [x, y] = eulerprove1(f, x0, y0, h, xn)
    x = x0:h:xn;
    y = zeros(size(x));
    y(1) = y0;
    for n = 1:length(x)-1
        k1 = f(x(n), y(n));
        k2 = f(x(n+1), y(n) + h*k1);
        y(n+1) = y(n) + h/2*(k1 + k2);
    end
end

f = @(x,y) y - 2*x/y;
[x, y] = euler1(f,0,1,0.1,1);
[xx, yy] = eulerprove1(f,0,1,0.1,1);
plot(x,double(yS(x)),'r*-',x,y,'b+-',xx,yy,'go-')
legend('analitique', 'euler', 'eulerprove');
